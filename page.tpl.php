<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<div id="page"> 
  <div id="header"> 
    <div id="headerimg"> 
      <?php if ($site_name) { ?><h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='slogan'><?php print $site_slogan ?></div><?php } ?>
    </div> 
    <?php if (isset($primary_links)) { ?><?php print kubrick_primary_links() ?><?php } ?>
  </div> 
  <hr /> 
  <div id="content" class="narrowcolumn"> 
    <?php print $header ?>
    <div class="navigation"> <?php print $breadcrumb ?> </div> 
    <div id="message"><?php print $messages ?></div> 
    <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
    <h2 class="page-title"><?php print $title ?></h2> 
    <?php print $tabs ?>
    <?php print $help ?>
    <!-- start main content --> 
    <?php print $content; ?>
    <!-- end main content --> 
  </div> 
  
  <?php if ($right) { ?>
    <div id="sidebar">
      <?php print $search_box ?>
      <br />
      <?php print $right ?>
    </div>
  <?php } ?>
  <hr /> 
  <div id="footer"> 
      <p><?php print $feed_icons; ?>&nbsp;<?php print $footer_message ?></p>
  </div> 
</div> 
<?php print $closure ?>
</body>
</html>
