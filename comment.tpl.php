<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
  <?php if ($new != '') { ?><a id="new"></a> <span class="new"><?php print $new; ?></span><?php } ?>
  <div style="float:right;">
    <?php if ($picture) {
    print $picture;
    } ?>
  </div>
  <cite><?php print $author ?> Says:</cite><br /> 
  <small><?php print $date ?></small>
  <div class="content"><?php print $content; ?></div> 
  <?php if ($picture) : ?> 
  <br class="clear" /> 
  <?php endif; ?> 
  <p class="postmetadata">&#187; <?php print $links ?></p> 
</div>
