Zubrick theme for phptemplate on Drupal

---------------------------------------

Last updated: 4 April 2008

This is a port of Kubrick theme to Drupal6, changed styles, and renamed as Zubrick to avoid conflicts.

These elements of phptemplate can be displayed (all are optional)
- header
	- site name
	- site slogan
	- primary link
- content
	- mission statement
- right sidebar
	- search form (optional)
- site footer

These elements will not be rendered
- site logo image
- secondary link

http://drupal.org/project/zubrick

Kubrick Authors:
- Gurpartap Singh (Current Project Maintainer for Drupal 4.7, 5)
- Chrisada Sookdhis (for Drupal 4.5, 4.6)

Zubrick Maintainer:
- Theodore karkoulis (coded drupal6 compatibility and theme changes)
