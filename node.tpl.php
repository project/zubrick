<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0) { ?><h2><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
  <div style="float:left;"><?php if ($picture) {
    print $picture;
  }?>
  </div>
  <small><?php if ($terms) { ?>Posted in <?php print $terms?> | <?php }; ?><?php if ($links) { ?><?php print $links?> &#187;</p><?php }; ?></small>
  <div class="entry"><?php print $content?></div>
  <small><?php print $submitted?></small>
</div>
