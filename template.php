<?php

function kubrick_regions() {
  return array(
       'header'  => t('header'),
       'right'   => t('right sidebar'),
       'content' => t('content'),
       'footer'  => t('footer')
  );
}

function kubrick_primary_links() {
  $links = menu_primary_links();
  if ($links) {
    $output .= '<ul id="primarylink">';
    foreach ($links as $link) {
      $link = l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']);
      $output .= '<li>' . $link . '</li>';
    }; 
    $output .= '</ul>';
  }
  return $output;
}
